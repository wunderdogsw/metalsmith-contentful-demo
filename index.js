var Metalsmith  = require('metalsmith'),
    markdown    = require('metalsmith-markdown'),
    templates   = require('metalsmith-templates'),
    collections = require('metalsmith-collections'),
    permalinks  = require('metalsmith-permalinks'),
    contentful  = require('contentful-metalsmith'),
    Handlebars  = require('handlebars'),
    debug       = require('metalsmith-debug'),
    fs          = require('fs'),
    metadata    = require('metalsmith-metadata'),
    feedreader  = require('./lib/metalsmith-feed-reader');


Handlebars.registerPartial('header', fs.readFileSync(__dirname + '/templates/partials/header.hbt').toString());
Handlebars.registerPartial('footer', fs.readFileSync(__dirname + '/templates/partials/footer.hbt').toString());
Handlebars.registerPartial('testimonial', fs.readFileSync(__dirname + '/templates/partials/testimonial.hbt').toString());


Metalsmith(__dirname)
  .use(collections({
    pages: {
        pattern: 'content/pages/*.md'
    },
    testimonials: {
        pattern: 'content/testimonials/*.md',
        sortBy: 'date',
        reverse: false
    }
  }))
  .use(metadata({
    global: 'config/globals.json',
  }))
  .use(markdown())
  .use(contentful({
    accessToken: '0c064949b482682c86ca44dbaf8b4c49087a7a5c2a9c5d36bf72d17ed1e8ed70'
  }))
  .use(feedreader())
  .use(templates('handlebars'))
  .clean(false)
  .build(function(err) {
    if (err) throw err;
  });
