Wunderdog.fi using metalsmith and contentful
============================================

# Prerequistics
- Node
- Contentful account credentials for managing content (not required for demo)

# Getting started
- Get static code generation running (watch for running build in loop)
```
brew install watch
npm install
npm run buildloop
npm run server
```

# How it works and would work in production
- Contentful, a cloud CMS system, manages content and only content
- Metalsmith, a static site generator, manages site structure
- Metalsmith is integrated to Contentful via a provided plugin (150 LOC)
- Options for generating site
  * On demand: When content changes, metalsmith fetches content from Contentful and publishes new static site (npm run build)
  * Scheduled: run (npm run build) on 5 minute interval

# Pros
- Separation of concerns -> Loose coupling with CMS system -> Small vendor lock-in
- Serving static site is simple and bullet-proof in runtime, because there are no moving parts. Just static html,css,images
- Relatively simple to setup. It took couple of hours to get this demo running.
- Contentful is free for "3 editors  + 1K content entries  + 100K API requests per month"
  - "To get access to all features, please upgrade to a paid subscription plan"
  - Paid subscription starts from 99$/month (company license)
    - "5 editors  + 5K content entries  + 1000K API requests per month"
- Contentful provides Google Authentication to the editor
- We can easily exhance Contentful to Flocker or any other API-driven CMS
- Metalsmith is a generic and pluggable framework for processing files
  - It does very little on it's own, it relies heavily on plugins
- Metalsmith is template-system agnostic

# Cons
- Metalsmith-contentful plugin has limited set of features
  - For example, lacking sorting of data
  - However, plugin can be easily extended or use a custom one
- Vendor lock-in to commercial product (Contentful)
- Requires some sort of scheduler (CI/cron)
- 'metalsmith-watch'-plugin doesn't seem to work with 'metalsmith-metadata'

# Links
- Metalsmith: http://metalsmith.io
- Metalsmith tutorial: http://www.robinthrift.com/posts/metalsmith-part-1-setting-up-the-forge/
- Contentful: https://www.contentful.com/
- Contentful metalsmith plugin: https://github.com/contentful-labs/contentful-metalsmith
- Contentful metalsmith example: https://github.com/contentful-labs/contentful-metalsmith-example
- Metalsmith multi languag: ehttps://www.npmjs.com/package/metalsmith-multi-language
