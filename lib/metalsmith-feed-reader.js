/* Simple plugin for demonstration purposes */
function plugin(options) {
  return function(files, metalsmith, done) {
    for (var file in files) {
      var fileMetadata = files[file];

      if (fileMetadata.feeds) {
        if (!fileMetadata.feeds.url) {
          throw new TypeError('Missing parameter: url');
        }
        //  For simplicity mock the feed
        //  Content is stored into this file' 'feeds' collection (array)
        fileMetadata.feeds.items = []
        fileMetadata.feeds.items.push({title: 'ARMv7 dedicated servers for 3.40 per month',
                                      link:'http://techcrunch.com/2015/09/02/scaleway-now-provides-crazy-cheap-virtual-private-servers-starting-at-3-40-per-month/'})
        fileMetadata.feeds.items.push({title: 'Solitary confinement is cruel and all too usual',
                                      link:'http://www.nytimes.com/2015/09/03/opinion/solitary-confinement-is-cruel-common-and-useless.html?_r=0'});

        done();
      }
    }
  }
};

module.exports = plugin;
